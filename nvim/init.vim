" 要安装的插件列表
call plug#begin('~/.vim/plugged')
" tab键补全功能插件
Plug 'rhysd/vim-clang-format'
Plug 'ervandew/supertab'
" Coc 智能补全插件引擎
" Use release branch (recommend)
Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()

" set number signal
set nu
set shiftwidth=2
set softtabstop=2
set autoindent
