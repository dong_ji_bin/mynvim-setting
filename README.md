# mynvim-setting

#### 介绍
neovim setting for myself;
style is so simple;
use .init.vim as config file.

## 命令

:PlugInstall
:CocInstall coc-clangd 
:call coc#util#install() # when report build/inderx.js not found, please install dependencies and compile coc.nvim : yarn install
:ClangFormat


